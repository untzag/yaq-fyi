{
    "config": {
        "limits": {
            "default": [
                -Infinity,
                Infinity
            ],
            "doc": "Configuration limits are strictly optional.",
            "items": "double",
            "type": "array"
        },
        "out_of_limits": {
            "default": "closest",
            "doc": "Control behavior of daemon when set_position is given a value outside of limits.",
            "name": "out_of_limits",
            "symbols": [
                "closest",
                "ignore",
                "error"
            ],
            "type": "enum"
        }
    },
    "doc": "The has-limits trait extends the has-positions trait with limits.\nIt's assumed that any position within the limits is valid.\n",
    "messages": {
        "get_destination": {
            "doc": "Get current daemon destination.",
            "origin": "has-position",
            "response": "double"
        },
        "get_limits": {
            "doc": "Get daemon limits.Limits will be the <a href='https://en.wikipedia.org/wiki/Intersection_(set_theory)'>intersection</a> of config limits and driver limits (when appliciable).",
            "response": {
                "items": "double",
                "type": "array"
            }
        },
        "get_position": {
            "doc": "Get current daemon position.",
            "origin": "has-position",
            "response": "double"
        },
        "get_units": {
            "doc": "Get units of daemon. These units apply to the position and destination fields.",
            "origin": "has-position",
            "response": [
                "null",
                "string"
            ]
        },
        "in_limits": {
            "doc": "Check if a given position is within daemon limits.",
            "request": [
                {
                    "name": "position",
                    "type": "double"
                }
            ],
            "response": "boolean"
        },
        "set_position": {
            "doc": "Give the daemon a new destination, and begin motion towards that destination.",
            "origin": "has-position",
            "request": [
                {
                    "name": "position",
                    "type": "double"
                }
            ]
        },
        "set_relative": {
            "doc": "Give the daemon a new destination relative to its current position. Daemon will immediately begin motion towards new destination. Returns new destination.",
            "origin": "has-position",
            "request": [
                {
                    "name": "distance",
                    "type": "double"
                }
            ],
            "response": "double"
        }
    },
    "requires": [
        "has-position"
    ],
    "state": {
        "destination": {
            "default": NaN,
            "origin": "has-position",
            "type": "double"
        },
        "hw_limits": {
            "default": [
                -Infinity,
                Infinity
            ],
            "items": "double",
            "type": "array"
        },
        "position": {
            "default": NaN,
            "origin": "has-position",
            "type": "double"
        }
    },
    "trait": "has-limits"
}
